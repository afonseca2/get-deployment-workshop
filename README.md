# GET Deployment Workshop
:wave: Welcome to the GET deployment workshop :fox:

This is the hands on learning portion of the [certified implementation engineer specialist](https://about.gitlab.com/services/pse-certifications/implementation-specialist/) learning journey. The learning in this workshop will help you deliver GitLab Implementation Services leveraging the  [implementation services delivery kit](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/implementation-template)

To start, create an issue from the [deployment-workshop issue template](https://gitlab.com/gitlab-org/professional-services-automation/tools/implementation/GET-deployment-workshop/-/issues/new?issuable_template=aws-3k-deployment-workshop) and follow the instructions to completion. 
